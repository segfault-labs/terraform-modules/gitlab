<!-- BEGIN_AUTOMATED_TF_DOCS_BLOCK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | ~> 16.0 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | ~> 2.9 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.21 |

Basic usage of this module is as follows:

```hcl
module "example" {
	 source  = "<module-path>"

	 # Required variables
	 agent_name  = 
	 project_path  = 

	 # Optional variables
	 agent_chart_version  = "1.16.0"
	 agent_config  = null
	 agent_version  = null
	 author_email  = "terraform@example.com"
	 author_name  = "Terraform"
	 create_namespace  = false
	 create_project  = false
	 gitlab_kas_address  = "wss://kas.gitlab.com"
	 groups  = []
	 kube_namespace  = "gitlab-agent"
	 project_namespace  = null
	 projects  = []
	 token_description  = "Token for KAS Agent Authentication"
	 token_name  = "kas-token"
}
```

## Resources

| Name | Type |
|------|------|
| [gitlab_cluster_agent.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/cluster_agent) | resource |
| [gitlab_cluster_agent_token.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/cluster_agent_token) | resource |
| [gitlab_project.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project) | resource |
| [gitlab_repository_file.agent_config](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/repository_file) | resource |
| [helm_release.gitlab_agent](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubernetes_namespace.gitlab_agent](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [gitlab_group.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/group) | data source |
| [gitlab_project.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/project) | data source |
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_agent_chart_version"></a> [agent\_chart\_version](#input\_agent\_chart\_version) | n/a | `string` | `"1.16.0"` | no |
| <a name="input_agent_config"></a> [agent\_config](#input\_agent\_config) | n/a | `any` | `null` | no |
| <a name="input_agent_name"></a> [agent\_name](#input\_agent\_name) | n/a | `string` | n/a | yes |
| <a name="input_agent_version"></a> [agent\_version](#input\_agent\_version) | n/a | `string` | `null` | no |
| <a name="input_author_email"></a> [author\_email](#input\_author\_email) | n/a | `string` | `"terraform@example.com"` | no |
| <a name="input_author_name"></a> [author\_name](#input\_author\_name) | n/a | `string` | `"Terraform"` | no |
| <a name="input_create_namespace"></a> [create\_namespace](#input\_create\_namespace) | n/a | `bool` | `false` | no |
| <a name="input_create_project"></a> [create\_project](#input\_create\_project) | n/a | `bool` | `false` | no |
| <a name="input_gitlab_kas_address"></a> [gitlab\_kas\_address](#input\_gitlab\_kas\_address) | n/a | `string` | `"wss://kas.gitlab.com"` | no |
| <a name="input_groups"></a> [groups](#input\_groups) | n/a | `list(string)` | `[]` | no |
| <a name="input_kube_namespace"></a> [kube\_namespace](#input\_kube\_namespace) | n/a | `string` | `"gitlab-agent"` | no |
| <a name="input_project_namespace"></a> [project\_namespace](#input\_project\_namespace) | n/a | `string` | `null` | no |
| <a name="input_project_path"></a> [project\_path](#input\_project\_path) | n/a | `string` | n/a | yes |
| <a name="input_projects"></a> [projects](#input\_projects) | n/a | `list(string)` | `[]` | no |
| <a name="input_token_description"></a> [token\_description](#input\_token\_description) | n/a | `string` | `"Token for KAS Agent Authentication"` | no |
| <a name="input_token_name"></a> [token\_name](#input\_token\_name) | n/a | `string` | `"kas-token"` | no |
## Outputs

No outputs.
<!-- END_AUTOMATED_TF_DOCS_BLOCK -->