locals {
  agent_config = var.agent_config == null ? {
    ci_access = {
      projects = [for project in var.projects : { id : project }]
      groups   = [for group in var.groups : { id : group }]
    }
  } : var.agent_config
}

data "gitlab_project" "this" {
  path_with_namespace = var.create_project ? gitlab_project.this[0].path_with_namespace : "${var.project_namespace}/${var.project_path}"
}

data "gitlab_group" "this" {
  count     = var.create_project ? 1 : 0
  full_path = var.project_namespace
}

resource "gitlab_project" "this" {
  count = var.create_project ? 1 : 0
  name  = var.project_path
  path  = var.project_path

  namespace_id = data.gitlab_group.this[0].id

  skip_wait_for_default_branch_protection = true
}

resource "gitlab_cluster_agent" "this" {
  project = data.gitlab_project.this.id
  name    = var.agent_name
}

resource "gitlab_cluster_agent_token" "this" {
  project     = data.gitlab_project.this.id
  agent_id    = gitlab_cluster_agent.this.agent_id
  name        = var.token_name == "" ? var.agent_name : var.token_name
  description = var.token_description
}

resource "gitlab_repository_file" "agent_config" {
  project        = gitlab_cluster_agent.this.project
  branch         = data.gitlab_project.this.default_branch
  file_path      = ".gitlab/agents/${gitlab_cluster_agent.this.name}/config.yaml"
  content        = base64encode(yamlencode(local.agent_config))
  author_email   = var.author_email
  author_name    = var.author_name
  commit_message = "feature: add/update agent config for ${gitlab_cluster_agent.this.name}"
}

resource "kubernetes_namespace" "gitlab_agent" {
  count = var.create_namespace ? 1 : 0
  metadata {
    name = var.kube_namespace
  }
}

resource "helm_release" "gitlab_agent" {
  name      = "gitlab-agent"
  namespace = var.create_namespace ? kubernetes_namespace.gitlab_agent[0].metadata[0].name : var.kube_namespace

  repository = "https://charts.gitlab.io"
  chart      = "gitlab-agent"
  version    = var.agent_chart_version

  create_namespace = false

  set_sensitive {
    name  = "config.token"
    value = gitlab_cluster_agent_token.this.token
  }

  dynamic "set" {
    for_each = var.agent_version == null ? {} : { yes = "1" }
    content {
      name  = "image.tag"
      value = var.agent_version
    }
  }

  values = [<<YAML
config:
  kasAddress: "${var.gitlab_kas_address}"
YAML
  ]
}
