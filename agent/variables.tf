variable "project_path" {
  type = string
}

variable "project_namespace" {
  type    = string
  default = null
}

variable "create_project" {
  type    = bool
  default = false
}

variable "agent_name" {
  type = string
}

variable "agent_chart_version" {
  type    = string
  default = "1.16.0"
}

variable "agent_version" {
  type    = string
  default = null
}

variable "token_name" {
  type    = string
  default = "kas-token"
}

variable "create_namespace" {
  type    = bool
  default = false
}

variable "kube_namespace" {
  type    = string
  default = "gitlab-agent"
}

variable "token_description" {
  type    = string
  default = "Token for KAS Agent Authentication"
}

variable "gitlab_kas_address" {
  type    = string
  default = "wss://kas.gitlab.com"
}

variable "author_email" {
  type    = string
  default = "terraform@example.com"
}

variable "author_name" {
  type    = string
  default = "Terraform"
}

variable "projects" {
  type    = list(string)
  default = []
}

variable "groups" {
  type    = list(string)
  default = []
}

variable "agent_config" {
  type    = any
  default = null
}
