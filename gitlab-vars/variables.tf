variable "variables_map" {
  type    = map(string)
  default = {}
}

variable "scope" {
  type    = string
  default = "*"
}

variable "masked" {
  type    = bool
  default = true
}

variable "project_path" {
  type = string
}

variable "protected" {
  type    = bool
  default = true
}

variable "type" {
  type    = string
  default = "env_var"
  validation {
    error_message = "Allowed values for variable type are: file | env_var"
    condition     = can(regex("^(env_var|file)$", var.type))
  }
}
