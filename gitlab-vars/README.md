<!-- BEGIN_AUTOMATED_TF_DOCS_BLOCK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.1, < 2.0 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | >= 15.7, < 17.0 |

Basic usage of this module is as follows:

```hcl
module "example" {
	 source  = "<module-path>"

	 # Required variables
	 project_path  = 

	 # Optional variables
	 masked  = true
	 protected  = true
	 scope  = "*"
	 type  = "env_var"
	 variables_map  = {}
}
```

## Resources

| Name | Type |
|------|------|
| [gitlab_project_variable.this](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_masked"></a> [masked](#input\_masked) | n/a | `bool` | `true` | no |
| <a name="input_project_path"></a> [project\_path](#input\_project\_path) | n/a | `string` | n/a | yes |
| <a name="input_protected"></a> [protected](#input\_protected) | n/a | `bool` | `true` | no |
| <a name="input_scope"></a> [scope](#input\_scope) | n/a | `string` | `"*"` | no |
| <a name="input_type"></a> [type](#input\_type) | n/a | `string` | `"env_var"` | no |
| <a name="input_variables_map"></a> [variables\_map](#input\_variables\_map) | n/a | `map(string)` | `{}` | no |
## Outputs

No outputs.
<!-- END_AUTOMATED_TF_DOCS_BLOCK -->