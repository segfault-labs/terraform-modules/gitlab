resource "gitlab_project_variable" "this" {
  for_each          = var.variables_map
  environment_scope = var.scope
  key               = each.key
  masked            = var.masked
  project           = var.project_path
  protected         = var.protected
  value             = each.value
  variable_type     = var.type
}
