data "gitlab_project" "this" {
  path_with_namespace = var.create_project ? gitlab_project.this[0].path_with_namespace : var.project_path
}

resource "gitlab_project" "this" {
  count = var.create_project ? 1 : 0
  name  = var.project_name
  path  = var.project_path
}

resource "gitlab_cluster_agent" "this" {
  project = data.gitlab_project.this.id
  name    = var.agent_name
}

resource "gitlab_cluster_agent_token" "this" {
  project     = data.gitlab_project.this.id
  agent_id    = gitlab_cluster_agent.this.agent_id
  name        = var.token_name == "" ? var.agent_name : var.token_name
  description = var.token_description
}

resource "gitlab_repository_file" "agent_config" {
  project   = gitlab_cluster_agent.this.project
  branch    = data.gitlab_project.this.default_branch
  file_path = ".gitlab/agents/${gitlab_cluster_agent.this.name}/config.yaml"
  content = base64encode(<<EOT
ci_access:
  projects:
%{for project in var.projects~}
    - id: ${project}
%{endfor~}
  groups:
%{for group in var.groups~}
    - id: ${group}
%{endfor~}
EOT
  )
  author_email   = var.author_email
  author_name    = var.author_name
  commit_message = "feature: add/update agent config for ${gitlab_cluster_agent.this.name}"
}

resource "kubernetes_namespace" "gitlab_agent" {
  metadata {
    name = var.namespace
  }
}

resource "helm_release" "gitlab_agent" {
  name      = "gitlab-agent"
  namespace = kubernetes_namespace.gitlab_agent.metadata[0].name

  repository = "https://charts.gitlab.io"
  chart      = "gitlab-agent"
  version    = "1.24.0"

  create_namespace = false

  set {
    name  = "config.token"
    value = gitlab_cluster_agent_token.this.token
  }

  values = [<<YAML
image:
  tag: ${var.agent_version}
config:
  kasAddress: "${var.gitlab_kas_address}"
YAML
  ]

}
