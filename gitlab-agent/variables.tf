variable "project_path" {
  type = string
}

variable "project_name" {
  type = string
}

variable "create_project" {
  type    = bool
  default = false
}

variable "agent_name" {
  type = string
}

variable "agent_version" {
  type    = string
  default = "v16.3.0"
}

variable "token_name" {
  type    = string
  default = "kas-token"
}

variable "namespace" {
  type    = string
  default = "gitlab-agent"
}

variable "token_description" {
  type    = string
  default = "Token for KAS Agent Authentication"
}

variable "gitlab_kas_address" {
  type    = string
  default = "wss://kas.gitlab.com"
}

variable "author_email" {
  type    = string
  default = "terraform@example.com"
}

variable "author_name" {
  type    = string
  default = "Terraform"
}

variable "projects" {
  type    = list(string)
  default = []
}

variable "groups" {
  type    = list(string)
  default = []
}
