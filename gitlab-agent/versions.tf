terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.17"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.8"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16.0"
    }
  }
  required_version = "~> 1.0"
}

